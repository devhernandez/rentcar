﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RentCar.Models;

namespace RentCar.Controllers
{
    public class TiposVehiculosController : Controller
    {
        private RentCarDBContest db = new RentCarDBContest();

        // GET: TiposVehiculos
        public ActionResult Index()
        {
            return View(db.TiposVehiculos.ToList());
        }

        // GET: TiposVehiculos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposVehiculos tiposVehiculos = db.TiposVehiculos.Find(id);
            if (tiposVehiculos == null)
            {
                return HttpNotFound();
            }
            return View(tiposVehiculos);
        }

        // GET: TiposVehiculos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposVehiculos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre")] TiposVehiculos tiposVehiculos)
        {
            if (ModelState.IsValid)
            {
                db.TiposVehiculos.Add(tiposVehiculos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tiposVehiculos);
        }

        // GET: TiposVehiculos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposVehiculos tiposVehiculos = db.TiposVehiculos.Find(id);
            if (tiposVehiculos == null)
            {
                return HttpNotFound();
            }
            return View(tiposVehiculos);
        }

        // POST: TiposVehiculos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] TiposVehiculos tiposVehiculos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tiposVehiculos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tiposVehiculos);
        }

        // GET: TiposVehiculos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposVehiculos tiposVehiculos = db.TiposVehiculos.Find(id);
            if (tiposVehiculos == null)
            {
                return HttpNotFound();
            }
            return View(tiposVehiculos);
        }

        // POST: TiposVehiculos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TiposVehiculos tiposVehiculos = db.TiposVehiculos.Find(id);
            db.TiposVehiculos.Remove(tiposVehiculos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
