﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RentCar.Models;

namespace RentCar.Controllers
{
    public class TiposCombutiblesController : Controller
    {
        private RentCarDBContest db = new RentCarDBContest();

        // GET: TiposCombutibles
        public ActionResult Index()
        {
            return View(db.TiposCombutibles.ToList());
        }

        // GET: TiposCombutibles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposCombutibles tiposCombutibles = db.TiposCombutibles.Find(id);
            if (tiposCombutibles == null)
            {
                return HttpNotFound();
            }
            return View(tiposCombutibles);
        }

        // GET: TiposCombutibles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposCombutibles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre")] TiposCombutibles tiposCombutibles)
        {
            if (ModelState.IsValid)
            {
                db.TiposCombutibles.Add(tiposCombutibles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tiposCombutibles);
        }

        // GET: TiposCombutibles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposCombutibles tiposCombutibles = db.TiposCombutibles.Find(id);
            if (tiposCombutibles == null)
            {
                return HttpNotFound();
            }
            return View(tiposCombutibles);
        }

        // POST: TiposCombutibles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] TiposCombutibles tiposCombutibles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tiposCombutibles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tiposCombutibles);
        }

        // GET: TiposCombutibles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposCombutibles tiposCombutibles = db.TiposCombutibles.Find(id);
            if (tiposCombutibles == null)
            {
                return HttpNotFound();
            }
            return View(tiposCombutibles);
        }

        // POST: TiposCombutibles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TiposCombutibles tiposCombutibles = db.TiposCombutibles.Find(id);
            db.TiposCombutibles.Remove(tiposCombutibles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
