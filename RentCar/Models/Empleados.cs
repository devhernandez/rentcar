﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentCar.Models
{
    public class Empleados
    {

        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Cedula { get; set; }

        public string Telefono { get; set; }

        public int PorcientoComision { get; set; }

        public DateTime FechaIngreso { get; set; }

        public DateTime FechaNacimiento { get; set; }

    }

}