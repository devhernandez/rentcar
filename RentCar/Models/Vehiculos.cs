﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentCar.Models
{
    public class Vehiculos
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string No_Chasis { get; set; }

        public string No_Motor { get; set; }

        public string No_Placa { get; set; }

        public Marcas Marca { get; set; }

        public Modelos Modelo { get; set; }

        public TiposCombutibles TipoCombustible { get; set; }

        public TiposVehiculos TipoVehiculo { get; set; }

        public string Anio { get; set; }

        public string Color { get; set; }

        public decimal Tarifa { get; set; }

    }

}