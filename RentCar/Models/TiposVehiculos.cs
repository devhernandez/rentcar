﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentCar.Models
{
    public class TiposVehiculos
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }

    }

}