﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentCar.Models
{
    public class Reservaciones
    {

        [Key]
        public int Id { get; set; }
        public Empleados Empleado { get; set; }

        public Vehiculos Vehiculo { get; set; }

        public Clientes Cliente { get; set; }

        public DateTime FechaRenta { get; set; }

        public DateTime FechaDevolucion { get; set; }

        public string Comentario { get; set; }
    }
}