﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentCar.Models
{
    public class Clientes
    {
         
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Cedula { get; set; }

        public string No_TarjetaCredito { get; set; }

        public string TipoPersona { get; set; }

        public string Sexo { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public string Direccion { get; set; }

   

    }
}