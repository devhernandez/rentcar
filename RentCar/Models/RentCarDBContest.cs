﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentCar.Models
{
    public class RentCarDBContest : DbContext
    {
        public DbSet<Marcas> Marcas { get; set; }
        public DbSet<Modelos> Modelos { get; set; }
        public DbSet<TiposCombutibles> TiposCombutibles { get; set; }
        public DbSet<TiposVehiculos> TiposVehiculos { get; set; }
        public DbSet<Vehiculos> Vehiculos { get; set; }
        public DbSet<Clientes> Clientes { get; set; }
        public DbSet<Empleados> Empleados { get; set; }
        public DbSet<Reservaciones> Reservaciones { get; set; }
    }
}